from time import sleep
from .mail import get_unread_messages, unsubscribe

# Indicates how often to check if there are new messages (in seconds)
TIMEOUT = 60

while True:
    for message in get_unread_messages():
        try:
            unsubscribe(message)
        except:
            pass

    sleep(TIMEOUT)
