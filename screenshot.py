# import PIL.ImageGrab
import pyautogui
import pyperclip
from pynput import keyboard


def on_release(key):
    if hasattr(key, 'char') and key.char.lower() == 'c':
        print("clipboard: {}".format(pyperclip.paste()))
        # Stop listener
        return False


with keyboard.Listener(on_release=on_release) as listener:
    listener.join()


# x, y = pyautogui.position()


# def on_click(x, y, button, pressed):
#     print('{0} at {1}'.format(
#         'Pressed' if pressed else 'Released',
#         (x, y)))
#     if not pressed:
#         take_screenshot(x, y)

#         # Stop listener
#         return False


# def take_screenshot(x, y, w=100, h=100):
#     bbox = (x, y, x + w, y + h)
#     img = PIL.ImageGrab.grab(bbox=bbox)
#     img.show()


# with mouse.Listener(on_click=on_click) as listener:
#     listener.join()
