import os.path
import datetime
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials

SCOPES = ['https://www.googleapis.com/auth/gmail.modify',
          'https://www.googleapis.com/auth/gmail.settings.basic']


def authenticate():
    creds = None
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.json'):
        creds = Credentials.from_authorized_user_file('token.json', SCOPES)

    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.json', 'w') as token:
            token.write(creds.to_json())

    return creds


def get_unread_messages():
    results = service.users().messages().list(userId='me', q="is:unread").execute()
    return results.get('messages', [])


def get_message_details(message_id):
    return service.users().messages().get(userId="me", id=message_id).execute()


def get_subject(message_payload):
    for header in message_payload['headers']:
        if header['name'] == "Subject":
            return header['value']


def unsubscribe(message, until=None):
    """Creates a new filter that marks all messages with the same subject as the given
    `message` as read. Optionally pass an expiration date for the filter in the format
    "mm/dd/yyyy".
    Returns the newly created filter ID
    """
    message_details = get_message_details(message['id'])
    subject = get_subject(message_details['payload'])
    return unsubscribe_from_subject(subject, until=until)


def unsubscribe_from_subject(subject, until=None):
    """Creates a new filter that marks all messages with the given `subject` as read.
    Optionally pass an expiration date for the filter in the format "mm/dd/yyyy". 
    Returns the newly created filter ID
    """

    if until is None:
        tomorrow = datetime.datetime.now() + datetime.timedelta(days=1)
        until = tomorrow.strftime('%m/%d/%Y')

    query = 'before:%s' % (until) if until != 0 else ''

    body = {
        'criteria': {
            'subject': subject,
            'query': query
        },
        'action': {
            'removeLabelIds': ['UNREAD']
        }
    }
    print('creating filter', body)
    result = service.users().settings().filters()\
        .create(userId='me', body=body).execute()
    print('created filter %s' % (result.get('id')))
    return result.get('id')


creds = authenticate()
service = build('gmail', 'v1', credentials=creds)
