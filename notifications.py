import Foundation
import objc
import AppKit

NSUserNotification = objc.lookUpClass('NSUserNotification')
NSUserNotificationCenter = objc.lookUpClass('NSUserNotificationCenter')

def notify(title, subtitle, delay=0):
  notification = NSUserNotification.alloc().init()
  notification.setTitle_(title)
  notification.setSubtitle_(subtitle)
  notification.setUserInfo_({})
  # notification.setUserInfo_({"action":"open_url", "value":url})

  delivery_date = Foundation.NSDate.dateWithTimeInterval_sinceDate_(delay, Foundation.NSDate.date())
  notification.setDeliveryDate_(delivery_date)

  NSUserNotificationCenter.defaultUserNotificationCenter()\
    .scheduleNotification_(notification)

notify("Test", "Test")